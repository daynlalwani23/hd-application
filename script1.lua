local Roblox_Materials = {

	Enum.Material.Brick,
	Enum.Material.Cobblestone,
	Enum.Material.Concrete,
	Enum.Material.CorrodedMetal,
	Enum.Material.DiamondPlate,
	Enum.Material.Fabric,
	Enum.Material.Foil,
	Enum.Material.ForceField,
	Enum.Material.Glass,
	Enum.Material.Granite,
	Enum.Material.Grass,
	Enum.Material.Ice,
	Enum.Material.Marble,
	Enum.Material.Metal,
	Enum.Material.Neon,
	Enum.Material.Pebble,
	Enum.Material.Plastic,
	Enum.Material.Sand,
	Enum.Material.Slate,
	Enum.Material.SmoothPlastic,
	Enum.Material.Wood,
	Enum.Material.WoodPlanks

}

local SHAPES = {
	
	Enum.PartType.Ball,
	Enum.PartType.Block,
	Enum.PartType.Wedge,
	Enum.PartType.Cylinder,
	Enum.PartType.CornerWedge

	
	
	
	
}

script.Parent.RemoteEvent.OnServerEvent:Connect(function(plr)
	local DS = game:GetService("DataStoreService")
	local DB = DS:GetDataStore("Saves")
	local sv = "S1"..tostring(plr.UserId)


	local je = game.Workspace:GetChildren()
	for i in pairs(je) do
		if je[i].Name ~= "Terrain" then
			if je[i]:FindFirstChild("Humanoid") == nil then
				if je[i].Name == "Part" then
					local item = game.Workspace:FindFirstChild(je[i].Name,true)
					item:Destroy()
				end
				
			end
			
		end
			
	end
	
	local lst = DB:GetAsync(sv)
	
	for a in ipairs(lst) do
		
		local objinfo = lst[a]
		
		local newp = Instance.new("Part")
		
		newp.Position = Vector3.new(objinfo[1],objinfo[2],objinfo[3])
		newp.Rotation = Vector3.new(objinfo[4],objinfo[5],objinfo[6])
		newp.Color = Color3.new(objinfo[7],objinfo[8],objinfo[9])
		newp.Size = Vector3.new(objinfo[10],objinfo[11],objinfo[12]) 
		
		newp.Parent = game.Workspace
		newp.Anchored = objinfo[14]
		
		for i in pairs(Roblox_Materials) do
			
			if objinfo[13] == Roblox_Materials[i].Name then
				
				newp.Material = Roblox_Materials[i]
				
			end
			
		end
		
		-- Shape
		
		for i in pairs(SHAPES) do

			if objinfo[15] == SHAPES[i].Name then

				newp.Shape = SHAPES[i]

			end

		end
		
	end
	
end)
		

